package ui;

public interface SimulatorUI {
    void greetAndShowMap();
    void getUserCommand();
    void presentCostAndExit();
    boolean shouldTerminate();
}

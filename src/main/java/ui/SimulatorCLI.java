package ui;

import bulldozer.BulldozerAPI;
import bulldozer.BulldozerExpense;
import bulldozer.BulldozerSimulator;
import expense.RateCalculator;
import grid.InvalidInputException;
import land.DestroyedPreservedException;
import land.LandBlockTypeAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class SimulatorCLI implements SimulatorUI{


    BufferedReader userInput;
    BulldozerAPI bulldozer;
    private LandBlockTypeAdapter[][] siteMap;
    private boolean shouldTerminate;
    private List<String> givenCommands;
    private boolean trackLastCommand;

    public SimulatorCLI(LandBlockTypeAdapter[][] siteMap){
        InputStreamReader inputStreamReader = new InputStreamReader(System.in);
        userInput = new BufferedReader(inputStreamReader);
        bulldozer = new BulldozerSimulator();
        shouldTerminate = false;
        givenCommands = new ArrayList<>();
        trackLastCommand = false;
        loadMap(siteMap);
    }

    private void loadMap(LandBlockTypeAdapter[][] siteMap) {
        this.siteMap = siteMap;
        bulldozer.loadSiteMap(siteMap);
    }

    @Override
    public void greetAndShowMap() {
        System.out.println(SimulatorCLIMessages.greetingMessage);
        System.out.println(SimulatorCLIMessages.showMap);
        StringBuilder map = new StringBuilder();
        for (LandBlockTypeAdapter[] line : siteMap) {
            for (LandBlockTypeAdapter cell : line) {
                map.append(cell);
            }
            map.append(SimulatorCLIMessages.newLine);
        }
        System.out.println(map.toString());
        System.out.println(SimulatorCLIMessages.bulldozerInitStatus);
        System.out.println();
    }

    @Override
    public void getUserCommand() {
        System.out.println(SimulatorCLIMessages.commandOptions);
        String userCommand = null;
        String[] charCommands = null;
        try {
            userCommand = userInput.readLine();
            charCommands = userCommand.split(" ");
        } catch (IOException | NullPointerException e) {
            System.out.println(SimulatorCLIMessages.errorUnknownCommand);
            return;
        }

        parseCommand(charCommands);
        if (trackLastCommand) {
            givenCommands.add(userCommand);
        }
    }

    private void parseCommand(String[] charCommands) {
        switch (charCommands[0]) {
            case SimulatorCLIMessages.turnLeft:
                bulldozer.turnLeft();
                trackLastCommand = true;
                break;
            case SimulatorCLIMessages.turnRight:
                bulldozer.turnRight();
                trackLastCommand = true;
                break;
            case SimulatorCLIMessages.advance:
                tryToAdvance(charCommands);
                break;
            case SimulatorCLIMessages.quit:
                shouldTerminate = true;
                trackLastCommand = true;
                break;
                default:
                    System.out.println(SimulatorCLIMessages.errorUnknownCommand);
                    trackLastCommand = false;
        }
    }

    private void tryToAdvance(String[] charCommands) {
        String stepsStr = charCommands[1];
        int steps;
        try {
            steps = Integer.parseInt(stepsStr);
            bulldozer.advance(steps);
            trackLastCommand = true;
        } catch (NumberFormatException e){
            System.out.println(String.format("%s - %s", SimulatorCLIMessages.errorSteps, stepsStr));
            trackLastCommand = false;
        } catch (IndexOutOfBoundsException e) {
            System.out.println(String.format("%s%s%s",
                    SimulatorCLIMessages.errorOutOfBound,
                    SimulatorCLIMessages.newLine,
                    SimulatorCLIMessages.terminating));
            shouldTerminate = true;
            trackLastCommand = true;
        } catch (InvalidInputException e){
            System.out.println(e.getMessage());
            trackLastCommand = false;
        } catch (DestroyedPreservedException e) {
            System.out.println(String.format("%s%s%s",
                    e.getMessage(),
                    SimulatorCLIMessages.newLine,
                    SimulatorCLIMessages.terminating));
            shouldTerminate = true;
            trackLastCommand = true;
        }
    }

    @Override
    public void presentCostAndExit() {
        RateCalculator<BulldozerExpense> expenseReport = bulldozer.getExpenseReport();
        printUsedCommands();
        System.out.println(SimulatorCLIMessages.showCosts);
        printExpenseReport(expenseReport);
        System.out.println(SimulatorCLIMessages.greetingEndMessage);
    }

    private void printUsedCommands() {
        StringBuilder commandBuilder = new StringBuilder();
        commandBuilder.append(SimulatorCLIMessages.newLine);
        commandBuilder.append(SimulatorCLIMessages.showCommandsUsed);
        commandBuilder.append(SimulatorCLIMessages.newLine);
        int i;
        for (i = 0; i < givenCommands.size() - 1; i++) {
            String givenCommand = givenCommands.get(i);
            String[] commandOnly = givenCommand.split("\\s+", 2);
            String fullCommand = SimulatorCLIMessages.getFullCommand(commandOnly[0]);
            commandBuilder.append(fullCommand);
            if (commandOnly.length > 1)
                commandBuilder.append(String.format(" %s", commandOnly[1]));
            commandBuilder.append(", ");
        }
        commandBuilder.append(givenCommands.get(i));
        commandBuilder.append(SimulatorCLIMessages.newLine);
        System.out.println(commandBuilder.toString());
    }

    private void printExpenseReport(RateCalculator<BulldozerExpense> expenseReport){
        StringBuilder expenseBuilder = new StringBuilder();
        int quantity;
        int cost;
        int totalCost = 0;
        expenseBuilder.append(SimulatorCLIMessages.newLine);
        String title = String.format(SimulatorCLIMessages.expenseFormat, "Item", "Quantity", "Cost");
        expenseBuilder.append(title);
        expenseBuilder.append(SimulatorCLIMessages.newLine);
        for (BulldozerExpense expense : expenseReport.getKeySet()) {
            String expenseStr = formatExpense(expense);
            quantity = expenseReport.getQuantity(expense);
            cost = expenseReport.getTotalCost(expense);
            totalCost += cost;
            expenseBuilder.append(String.format(SimulatorCLIMessages.expenseFormat, expenseStr, quantity, cost));
            expenseBuilder.append(SimulatorCLIMessages.newLine);
        }
        expenseBuilder.append("------------");
        expenseBuilder.append(SimulatorCLIMessages.newLine);
        String total = String.format(SimulatorCLIMessages.expenseFormat, "Total", "",totalCost);
        expenseBuilder.append(total);
        expenseBuilder.append(SimulatorCLIMessages.newLine);
        System.out.println(expenseBuilder.toString());
    }

    private String formatExpense(BulldozerExpense expense) {
        String expenseOrig = expense.toString();
        String withSpace = expenseOrig.replaceAll("(?<cap>[A-Z])", " ${cap}");
        String lowCase = withSpace.toLowerCase();
        return lowCase;
    }

    @Override
    public boolean shouldTerminate() {
        return shouldTerminate;
    }

    boolean isTrackLastCommand(){
        return trackLastCommand;
    }
}

package ui;

import land.LandBlockType;
import land.LandBlockTypeAdapter;
import land.TypeNotSupportedException;

import java.util.HashMap;
import java.util.Map;

public class CharToLandAdapter implements LandBlockTypeAdapter {
    final static Character plain = 'o';
    final static Character rocky = 'r';
    final static Character tree = 't';
    final static Character preservedTree = 'T';
    private final static Map<Character, LandBlockType> translationMap;

    static {
        translationMap = new HashMap<>();
        translationMap.put(plain, LandBlockType.Plain);
        translationMap.put(rocky, LandBlockType.Rocky);
        translationMap.put(tree, LandBlockType.Tree);
        translationMap.put(preservedTree, LandBlockType.PreservedTree);
    }

    private LandBlockType type;
    private Character typeChar;

    public CharToLandAdapter(char landChar) throws TypeNotSupportedException {
        typeChar = landChar;
        if (!translationMap.containsKey(landChar)) {
            throw new TypeNotSupportedException(String.format("Type %s not supported", landChar));
        }
        type = translationMap.get(landChar);
    }

    @Override
    public LandBlockType getLandBlockType() {
        return type;
    }

    @Override
    public String toString(){
        return typeChar.toString();
    }
}

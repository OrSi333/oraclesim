package ui;

import land.LandBlockTypeAdapter;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CharMapParser implements MapParser {

    BufferedReader mapReader;

    public CharMapParser(String filePath) throws FileNotFoundException {
        initMapReader(filePath);
    }

    private void initMapReader(String filePath) throws FileNotFoundException {
        File mapFile = new File(filePath);
        FileReader fileReader = null;
        fileReader = new FileReader(mapFile);
        mapReader = new BufferedReader(fileReader);
    }

    public LandBlockTypeAdapter[][] parseMap() throws IOException {
        CharToLandAdapter[][] charSiteMap = readMapFromFile(mapReader);
        return charSiteMap;
    }

    @Override
    public void close() throws Exception {
        mapReader.close();
    }

    private CharToLandAdapter[][] readMapFromFile(BufferedReader mapReader) throws IOException {
        CharToLandAdapter[][] charSiteMap = null;
        List<CharToLandAdapter[]> lines = new ArrayList<>();
        String currentLine;
        while ((currentLine = mapReader.readLine()) != null) {
            char[] lineChar = currentLine.toCharArray();
            CharToLandAdapter[] lineCharacter = toCharacterArray(lineChar);
            lines.add(lineCharacter);
        }


        charSiteMap = new CharToLandAdapter[lines.size()][];
        charSiteMap = lines.toArray(charSiteMap);
        return charSiteMap;
    }

    private CharToLandAdapter[] toCharacterArray(char[] lineChar) {
        CharToLandAdapter[] characterArray = new CharToLandAdapter[lineChar.length];
        List<CharToLandAdapter> lineList = new ArrayList<>();
        CharToLandAdapter currentAdapter;
        for (char c : lineChar) {
            currentAdapter = new CharToLandAdapter(c);
            lineList.add(currentAdapter);
        }
        return lineList.toArray(characterArray);
    }
}

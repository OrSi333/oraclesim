package ui;

import land.LandBlockTypeAdapter;

public interface MapParser {
    public LandBlockTypeAdapter[][] parseMap() throws Exception;
    public void close() throws Exception;
}

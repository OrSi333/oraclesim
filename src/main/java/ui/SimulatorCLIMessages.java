package ui;

import java.util.HashMap;
import java.util.Map;

public class SimulatorCLIMessages {
    static final String greetingMessage = "Welcome to the Aconex site clearing simulator.";
    static final String showMap = "This is a map of the site:";
    static final String bulldozerInitStatus = "The bulldozer is currently located at the Northern edge of the" +
            "site, immediately to the West of the site, and facing East.";
    static final String commandOptions = "(l)eft, (r)ight, (a)dvance <n>, (q)uit:";
    static final String showCommandsUsed = "The simulation has ended at your request. These are the bulldozer" +
            "you issued:";
    static final String showCosts = "The costs for this land clearing operation were:";
    static final String greetingEndMessage = "Thank you for using the Aconex site clearing simulator.";
    static final String newLine = System.getProperty("line.separator");

    static final String terminating = "Terminating...";
    static final String errorUnknownCommand = "Unknown command! try again";
    static final String errorSteps = "Unable to get number of steps! try again";
    static final String errorOutOfBound = "Bulldozer is out of bound!";

    static final String expenseFormat = "%-30s%-12s%s";

    static final String turnLeft = "l";
    static final String turnRight = "r";
    static final String advance = "a";
    static final String quit = "q";

    static final String turnLeftFull = "";
    static final String turnRightFull = "r";
    static final String advanceFull = "a";
    static final String quitFull = "q";

    static final Map<String, String> commandFullName;

    static {
        commandFullName = new HashMap<>();
        fillCommandsFullName();
    }

    private static void fillCommandsFullName() {
        commandFullName.put(turnRight, turnRightFull);
        commandFullName.put(turnLeft, turnLeftFull);
        commandFullName.put(advance, advanceFull);
        commandFullName.put(quit, quitFull);
    }

    public static String getFullCommand(String command){
        return commandFullName.get(command);
    }

}

import land.LandBlockTypeAdapter;
import log.FileLogger;
import log.Logger;
import log.TerminatorLoggerDecorator;
import ui.*;

import java.io.IOException;

public class Program {

    static final String mapNotProvided = "Map not provided";
    static final String logPath = "simulator.log";
    static String mapFilePath;
    static TerminatorLoggerDecorator logger;
    static SimulatorUI userInterface;

    public static void main(String[] args){
        initLogger();
        parseArgs(args);
        initCLI();
        userInterface.greetAndShowMap();
        while (!userInterface.shouldTerminate()){
            userInterface.getUserCommand();
        }
        userInterface.presentCostAndExit();
    }

    private static void initLogger(){
        Logger fileLogger = null;
        try {
            fileLogger = new FileLogger(logPath);
        } catch (IOException exception) { }
        finally {
            logger = new TerminatorLoggerDecorator(fileLogger);
        }
    }

    private static void parseArgs(String[] args) {
        if (args.length < 1) {
            logger.writeMessageToLog(mapNotProvided);
        }
        mapFilePath = args[0];
    }

    private static void initCLI(){
        try {
            MapParser mapParser = new CharMapParser(mapFilePath);
            LandBlockTypeAdapter[][] siteMap = mapParser.parseMap();
            userInterface = new SimulatorCLI(siteMap);
            mapParser.close();
        } catch (Exception e){
            logger.writeExceptionToLog(e);
        }
    }


}

package vehicle;

import grid.Grid;

public class VehicleBuilder implements DrivableBuilder {

    protected Vehicle vehicle;

    @Override
    public Drivable getDrivable() {
        return vehicle;
    }

    @Override
    public void reset() {
        vehicle = new Vehicle();
    }

    @Override
    public void setPosition() {
        Position position = new Position(0, -1, Direction.East);
        vehicle.setPosition(position);
    }

    @Override
    public void setGrid(Grid grid) {
        vehicle.setGrid(grid);
    }
}

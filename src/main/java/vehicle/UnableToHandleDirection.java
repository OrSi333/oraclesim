package vehicle;

public class UnableToHandleDirection extends RuntimeException {
    public UnableToHandleDirection(String message, Direction direction) {
        super(String.format("%s - %s", message, direction.toString()));
    }
}

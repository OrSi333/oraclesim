package vehicle;

public enum Direction {
    West(0),
    North(1),
    East(2),
    South(3);

    private int code;
    private static final Direction[] VALUES = values();


    Direction(int code) {
        this.code = code;
    }

    Direction Left() {
        int leftCode = (code + 3) % 4;
        return VALUES[leftCode];
    }

    Direction Right() {
        int rightCode = (code + 1) % 4;
        return VALUES[rightCode];
    }

}

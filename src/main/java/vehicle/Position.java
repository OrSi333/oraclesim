package vehicle;

import java.util.Objects;

@Struct
public class Position implements Cloneable{
    public int row;
    public int column;
    public Direction facing;

    public Position(int row, int column, Direction facing) {
        this.row = row;
        this.column = column;
        this.facing = facing;
    }

    @Override
    public Position clone(){
        return new Position(row, column, facing);
    }

    @Override
    public boolean equals(Object other){
        if (other instanceof Position){
            Position otherPosition = (Position)other;
            return row == otherPosition.row &&
                    column == otherPosition.column &&
                    facing == otherPosition.facing;
        }
        return false;
    }

    @Override
    public int hashCode(){
        return Objects.hash(row, column, facing);
    }
}

package vehicle;

import grid.Grid;

public class VehicleDirector {
    public VehicleDirector() {

    }

    public Drivable makeVehicle(VehicleBuilder builder, Grid grid) {
        builder.reset();
        builder.setPosition();
        builder.setGrid(grid);
        return builder.getDrivable();
    }
}

package vehicle;

import grid.Grid;
import land.LandBlock;

public class Vehicle implements Drivable {
    protected Position position;
    protected Grid grid;

    protected Vehicle(){
    }

    protected void setPosition(Position position) {
        this.position = position.clone();
    }

    protected void setGrid(Grid grid) {
        this.grid = grid;
    }

    @Override
    public void goForward(int steps) {
        for (int i = 0; i < steps; i++) {
            advanceSingleStep();
        }
        getCurrentBlock();
    }

    protected void advanceSingleStep(){
        switch (position.facing){
            case North:
                position.row--;
                break;
            case East:
                position.column++;
                break;
            case South:
                position.row++;
                break;
            case West:
                position.column--;
                break;
            default:
                throw new UnableToHandleDirection("Direction is not supported", position.facing);
        }
    }

    @Override
    public LandBlock getCurrentBlock(){
        return grid.getBlock(position.row, position.column);
    }

    @Override
    public Position getPosition() {
        return position.clone();
    }

    public void turnLeft() {
        position.facing = position.facing.Left();
    }

    public void turnRight() {
        position.facing = position.facing.Right();
    }

}

package vehicle;

import land.LandBlock;

public interface Drivable {
        public void goForward(int steps);
        public void turnLeft();
        public void turnRight();
        public LandBlock getCurrentBlock();
        public Position getPosition();
}

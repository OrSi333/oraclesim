package vehicle;

import grid.Grid;

public interface DrivableBuilder {
    void reset();
    void setPosition();
    void setGrid(Grid grid);
    Drivable getDrivable();
}

package bulldozer;

import expense.RatCalculatorImpl;
import expense.RateCalculator;
import land.LandBlockType;
import vehicle.VehicleBuilder;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class BulldozerBuilder extends VehicleBuilder {
    @Override
    public void reset(){
        vehicle = new Bulldozer();
    }

    void setFuelConsumption(){
        Bulldozer bulldozer = (Bulldozer)vehicle;
        Map<LandBlockType, Integer> fuelRate = new HashMap<>();
        fuelRate.put(LandBlockType.Clear, 1);
        fuelRate.put(LandBlockType.Plain, 1);
        fuelRate.put(LandBlockType.Rocky, 2);
        fuelRate.put(LandBlockType.Tree, 2);
        fuelRate.put(LandBlockType.PreservedTree, 2);
        bulldozer.setFuelConsumption(fuelRate);
    }

    void setExpenseCalculator(){
        Bulldozer bulldozer = (Bulldozer)vehicle;
        RateCalculator<BulldozerExpense> expenseCalculator = new RatCalculatorImpl<>();
        expenseCalculator.setPrice(BulldozerExpense.communicationOverhead, 1);
        expenseCalculator.setPrice(BulldozerExpense.fuelUsage, 1);
        expenseCalculator.setPrice(BulldozerExpense.unclearedSquares, 3);
        expenseCalculator.setPrice(BulldozerExpense.destructionOfProtected, 10);
        expenseCalculator.setPrice(BulldozerExpense.paintDamage, 2);
        bulldozer.setExpenseCalculator(expenseCalculator);
    }

    void setPaintScratchers(){
        Bulldozer bulldozer = (Bulldozer)vehicle;
        Set<LandBlockType> paintScratcherSet = new HashSet<>();
        paintScratcherSet.add(LandBlockType.Tree);
        paintScratcherSet.add(LandBlockType.PreservedTree);
        bulldozer.setPaintScratcher(paintScratcherSet);
    }
}

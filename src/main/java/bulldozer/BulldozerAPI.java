package bulldozer;

import expense.RateCalculator;
import land.LandBlockTypeAdapter;

public interface BulldozerAPI {
    void loadSiteMap(LandBlockTypeAdapter[][] siteMap);
    void advance(int steps);
    void turnRight();
    void turnLeft();
    RateCalculator<BulldozerExpense> getExpenseReport();
}

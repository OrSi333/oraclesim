package bulldozer;

import expense.RateCalculator;
import grid.Grid;
import grid.GridFactory;
import grid.GridFactoryImpl;
import land.LandBlockType;
import land.LandBlockTypeAdapter;
import vehicle.Drivable;

public class BulldozerSimulator implements BulldozerAPI {

    Bulldozer bulldozer;

    @Override
    public void loadSiteMap(LandBlockTypeAdapter[][] siteMap) {
        LandBlockType[][] landBlockSiteMap = convertSiteToLandBlock(siteMap);
        Grid grid = buildGrid(landBlockSiteMap);
        buildBulldozer(grid);
    }

    private LandBlockType[][] convertSiteToLandBlock(LandBlockTypeAdapter[][] siteMap) {
        LandBlockType[][] landBlockSiteMap = new LandBlockType[siteMap.length][];
        for (int row = 0; row < siteMap.length; row++) {
            landBlockSiteMap[row] = convertSiteRow(siteMap[row]);
        }
        return landBlockSiteMap;
    }

    private LandBlockType[] convertSiteRow(LandBlockTypeAdapter[] adapterRow) {
        LandBlockType[] landBlockRow = new LandBlockType[adapterRow.length];
        for (int column = 0; column < adapterRow.length; column++) {
            landBlockRow[column] = adapterRow[column].getLandBlockType();
        }
        return landBlockRow;
    }

    private Grid buildGrid(LandBlockType[][] siteMap) {
        GridFactory gridFactory = GridFactoryImpl.getInstance();
        Grid grid = gridFactory.buildGridFromTypes(siteMap);
        return grid;
    }

    private void buildBulldozer(Grid grid) {
        BulldozerDirector director = new BulldozerDirector();
        BulldozerBuilder builder = new BulldozerBuilder();
        director.makeBulldozer(builder, grid);
        Drivable drivable = builder.getDrivable();
        bulldozer = (Bulldozer)drivable;
    }

    @Override
    public void advance(int steps) {
        bulldozer.goForward(steps);
    }

    @Override
    public void turnRight() {
        bulldozer.turnRight();
    }

    @Override
    public void turnLeft() {
        bulldozer.turnLeft();
    }

    @Override
    public RateCalculator<BulldozerExpense> getExpenseReport() {
        RateCalculator<BulldozerExpense> expenseReport = bulldozer.getExpenseReport();
        return expenseReport;
    }
}

package bulldozer;

import grid.Grid;
import vehicle.VehicleDirector;

public class BulldozerDirector extends VehicleDirector {
    public void makeBulldozer(BulldozerBuilder builder, Grid grid) {
        builder.reset();
        builder.setPosition();
        builder.setGrid(grid);
        builder.setExpenseCalculator();
        builder.setFuelConsumption();
        builder.setPaintScratchers();
    }
}

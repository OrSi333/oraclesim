package bulldozer;

public enum BulldozerExpense {
    fuelUsage,
    communicationOverhead,
    unclearedSquares,
    destructionOfProtected,
    paintDamage
}

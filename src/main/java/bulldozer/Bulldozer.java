package bulldozer;

import expense.RateCalculator;
import grid.Grid;
import grid.InvalidInputException;
import land.LandBlock;
import land.LandBlockType;
import vehicle.Position;
import vehicle.Vehicle;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Bulldozer extends Vehicle {

    private Map<LandBlockType, Integer> fuelConsumption;
    private Set<LandBlockType> paintScratcherSet;
    private RateCalculator<BulldozerExpense> expenseCalculator;

    Bulldozer(){
        super();
    }

    void setFuelConsumption(Map<LandBlockType, Integer> fuelRate){
        fuelConsumption = new HashMap<>(fuelRate);
    }

    void setExpenseCalculator(RateCalculator<BulldozerExpense> expenseCalculator) {
        this.expenseCalculator = expenseCalculator;
    }

    public void setPaintScratcher(Set<LandBlockType> passThroughPenalty) {
        this.paintScratcherSet = new HashSet<>(passThroughPenalty);
    }

    @Override
    protected void setPosition(Position position) {
        super.setPosition(position);
    }

    @Override
    protected void setGrid(Grid grid) {
        super.setGrid(grid);
    }


    @Override
    public void goForward(int steps) {
        if (steps <= 0) {
            throw new InvalidInputException(String.format("Expected positive number, received %d", steps));
        }
        expenseCalculator.add(BulldozerExpense.communicationOverhead, 1);
        for (int currentStep = 0; currentStep < steps - 1; currentStep++) {
            advanceSingleStep();
            passThroughLand();
            clearBlock();
        }

        advanceSingleStep();
        clearBlock();

    }

    @Override
    public void turnLeft(){
        expenseCalculator.add(BulldozerExpense.communicationOverhead, 1);
        super.turnLeft();
    }

    @Override
    public void turnRight(){
        expenseCalculator.add(BulldozerExpense.communicationOverhead, 1);
        super.turnRight();
    }

    public RateCalculator<BulldozerExpense> getExpenseReport(){
        RateCalculator<BulldozerExpense> clone = expenseCalculator.clone();
        calculateUnclearedLand(clone);
        return clone;
    }

    private void calculateUnclearedLand(RateCalculator<BulldozerExpense> calculator) {
        int unclearLand = grid.calculateUnclearLand();
        calculator.add(BulldozerExpense.unclearedSquares, unclearLand);
    }

    protected void passThroughLand() {
        LandBlock block = getCurrentBlock();
        checkPaintDamage(block);
    }

    private void checkPaintDamage(LandBlock block) {
        LandBlockType type = block.getType();
        if (paintScratcherSet.contains(type)){
            expenseCalculator.add(BulldozerExpense.paintDamage, 1);
        }
    }

    protected void clearBlock() {
        LandBlock block = getCurrentBlock();
        addFuelCost(block);
        block.clear();

    }

    private void addFuelCost(LandBlock block) {
        LandBlockType type = block.getType();
        int fuelUnits = fuelConsumption.get(type);
        expenseCalculator.add(BulldozerExpense.fuelUsage, fuelUnits);
    }

}

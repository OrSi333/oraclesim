package land;

import java.util.HashSet;
import java.util.Set;

public class LandBlockFactoryImpl implements LandBlockFactory {

    Set<LandBlockType> supportedTypesList;
    private static LandBlockFactoryImpl instance;

    private LandBlockFactoryImpl(){
        supportedTypesList = new HashSet<>();
        putSupportedTypes();
    }

    public static LandBlockFactoryImpl getInstance(){
        if (instance == null) {
            instance = new LandBlockFactoryImpl();
        }
        return instance;
    }

    private void putSupportedTypes(){
        supportedTypesList.add(LandBlockType.Plain);
        supportedTypesList.add(LandBlockType.Rocky);
        supportedTypesList.add(LandBlockType.Tree);
        supportedTypesList.add(LandBlockType.PreservedTree);
    }

    @Override
    public LandBlock buildLandBlock(LandBlockType landBlockType) throws TypeNotSupportedException {
        switch (landBlockType){
            case Plain:
            case Rocky:
            case Tree:
                return new ClearableLandBlock(landBlockType);
            case PreservedTree:
                return new UnclearableLandBlock(landBlockType);
                default:
                    throw new TypeNotSupportedException(landBlockType);
        }
    }
}

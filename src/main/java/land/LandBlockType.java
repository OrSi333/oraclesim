package land;

public enum LandBlockType {
    Clear,
    Plain,
    Rocky,
    Tree,
    PreservedTree
}

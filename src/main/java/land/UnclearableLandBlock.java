package land;

public class UnclearableLandBlock implements LandBlock {

    private LandBlockType type;

    UnclearableLandBlock(LandBlockType type){
        this.type = type;
    }

    @Override
    public void clear() throws UnableToClearLandException {
        throw new DestroyedPreservedException(type);
    }

    @Override
    public boolean isClear() {
        return false;
    }

    @Override
    public boolean isClearable() {
        return false;
    }

    @Override
    public LandBlockType getType() {
        return type;
    }
}

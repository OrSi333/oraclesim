package land;

public class DestroyedPreservedException extends UnableToClearLandException {
    public DestroyedPreservedException(LandBlockType landBlockType){
        super(String.format("Destroyed protected %s", landBlockType.toString()));
    }
}

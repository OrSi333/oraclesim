package land;

public interface LandBlockFactory {
    public LandBlock buildLandBlock(LandBlockType landBlockType) throws TypeNotSupportedException;
}

package land;

public interface LandBlock{
    public void clear() throws UnableToClearLandException;
    public boolean isClear();
    public boolean isClearable();
    public LandBlockType getType();
}

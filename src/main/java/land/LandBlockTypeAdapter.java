package land;

public interface LandBlockTypeAdapter {
    LandBlockType getLandBlockType();
}

package land;

public class ClearableLandBlock implements LandBlock {

    private LandBlockType type;

    ClearableLandBlock(LandBlockType type) {
        this.type = type;
    }

    @Override
    public void clear() throws UnableToClearLandException {
        if (!isClearable()) {
            throw new UnableToClearLandException("This land is already clear!");
        }
        type = LandBlockType.Clear;
    }

    @Override
    public boolean isClear() {
        return type == LandBlockType.Clear;
    }

    @Override
    public boolean isClearable() {
        return !isClear();
    }

    @Override
    public LandBlockType getType() {
        return type;
    }
}

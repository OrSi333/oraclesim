package land;

public class UnableToClearLandException extends RuntimeException {
    public UnableToClearLandException(String errorMessage) {
        super(errorMessage);
    }
}
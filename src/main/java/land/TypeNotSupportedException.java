package land;

public class TypeNotSupportedException extends RuntimeException {
    public TypeNotSupportedException(LandBlockType landBlockType) {
        super(String.format("LandBlockType isn't supported - %s", landBlockType.toString()));
    }

    public TypeNotSupportedException(String message){
        super(message);
    }
}

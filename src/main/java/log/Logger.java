package log;

public interface Logger {
    public void writeMessageToLog(String message) throws Exception;
    public void writeExceptionToLog(Exception exception) throws Exception;
    public boolean ready();
    public void close() throws Exception;
}

package log;

public class TerminatorLoggerDecorator implements Logger {
    static final String terminating = "Error has occurred! Check log for more information. Terminating...";
    static final String emptyLog = "Failed initializing log";

    private Logger logger;

    public TerminatorLoggerDecorator(Logger logger){
        if (logger == null) {
            terminate(emptyLog);
        }
        this.logger = logger;
    }


    @Override
    public void writeMessageToLog(String message) throws RuntimeException {
        String exceptionMessage = "";
        try {
            logger.writeMessageToLog(message);
        } catch (Exception exception) {
            exceptionMessage = exception.getMessage();
        }
        terminate(exceptionMessage);
    }

    @Override
    public void writeExceptionToLog(Exception inputException) throws RuntimeException {
        try {
            logger.writeExceptionToLog(inputException);
        } catch (Exception exception) {
            terminate(exception.getMessage());
        }
        terminate(inputException.getMessage());
    }

    public void terminate(String message){
        System.out.println(message);
        System.out.println(terminating);
        System.exit(1);
    }

    @Override
    public boolean ready(){
        return logger.ready();
    }

    @Override
    public void close() throws RuntimeException {
        try {
            logger.close();
        } catch (Exception exception) {
            terminate(exception.getMessage());
        }
    }
}

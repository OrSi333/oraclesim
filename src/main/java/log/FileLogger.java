package log;

import java.io.*;

public class FileLogger implements Logger {

    private final static String logError = "Unable to write log";

    private BufferedWriter logWriter;
    private PrintWriter exceptionWriter;
    private final boolean printExceptionTree;
    private boolean isReady;

    public FileLogger(String filePath) throws IOException {
        this(new File(filePath));
    }

    public FileLogger(File logFile) throws IOException {
        initBufferedReader(logFile);
        initExceptionPrinter();
        this.printExceptionTree = true;
        isReady = true;
    }

    private void initBufferedReader(File logFile) throws IOException {
        FileWriter writer = new FileWriter(logFile);
        logWriter = new BufferedWriter(writer);
    }


    private void initExceptionPrinter(){
        StringWriter stringWriter = new StringWriter();
        boolean autoFlush = true;
        exceptionWriter = new PrintWriter(stringWriter, autoFlush);
    }

    @Override
    public void writeMessageToLog(String message) throws IOException {
        logWriter.write(message);
        logWriter.flush();
    }

    @Override
    public void writeExceptionToLog(Exception exception) throws IOException {
        String message = exception.getMessage();
        writeMessageToLog(message);
        if (printExceptionTree) {
            exception.printStackTrace(exceptionWriter);
            writeMessageToLog(exceptionWriter.toString());
        }
    }

    @Override
    public boolean ready(){
        return isReady;
    }

    @Override
    public void close() throws IOException {
        exceptionWriter.close();
        logWriter.flush();
        logWriter.close();
        isReady = false;
    }
}

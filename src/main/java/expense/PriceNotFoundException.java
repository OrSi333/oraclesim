package expense;

public class PriceNotFoundException extends RuntimeException {
    public <E>PriceNotFoundException(E element){
        super(String.format("Price not found for %s", element.toString()));
    }
}

package expense;

import java.util.Set;

public interface RateCalculator<E> extends Cloneable {
    void add(E element, int quantity);
    void setPrice(E element, int price);
    int getQuantity(E element);
    Set<E> getKeySet();
    int getTotalCost(E element);
    RateCalculator<E> clone();
}

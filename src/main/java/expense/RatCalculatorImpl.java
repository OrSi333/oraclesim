package expense;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class RatCalculatorImpl<E> implements RateCalculator<E> {

    protected Map<E, Integer> quantityMap;
    protected Map<E, Integer> priceMap;

    public RatCalculatorImpl(){
        quantityMap = new HashMap<>();
        priceMap = new HashMap<>();
    }

    protected RatCalculatorImpl(RatCalculatorImpl<E> other){
        quantityMap = new HashMap<>(other.quantityMap);
        priceMap = new HashMap<>(other.priceMap);
    }

    @Override
    public void add(E element, int quantity) {
        int newQuantity = 0;
        if (quantityMap.containsKey(element)) {
            newQuantity = quantityMap.get(element);
        }

        newQuantity += quantity;
        quantityMap.put(element, newQuantity);
    }

    @Override
    public void setPrice(E element, int price){
        priceMap.put(element, price);
    }

    @Override
    public int getQuantity(E element) {
        int quantity = 0;
        if (quantityMap.containsKey(element)) {
            quantity = quantityMap.get(element);
        }

        return quantity;
    }

    @Override
    public Set<E> getKeySet() {
        Set<E> priceSet = priceMap.keySet();
        return priceSet;
    }

    @Override
    public int getTotalCost(E element) throws PriceNotFoundException {
        int quantity = getQuantity(element);
        int price = 0;
        if (priceMap.containsKey(element)){
            price = priceMap.get(element);
        } else {
            throw new PriceNotFoundException(element);
        }
        return quantity * price;
    }

    @Override
    public RateCalculator<E> clone(){
        RateCalculator<E> clone = new RatCalculatorImpl<>(this);
        return clone;

    }
}

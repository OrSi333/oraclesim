package grid;

import land.LandBlock;
import land.LandBlockFactory;
import land.LandBlockFactoryImpl;
import land.LandBlockType;

public class GridFactoryImpl implements GridFactory {

    private static GridFactoryImpl gridFactory;

    private GridFactoryImpl(){

    }

    @Override
    public Grid buildGridFromTypes(LandBlockType[][] landBlockTypes) throws InvalidInputException {
        Grid grid;
        validateBlockTypeArray(landBlockTypes);
        LandBlock[][] blocks = initBlockArray2D(landBlockTypes);
        buildBlocksFromTypes(landBlockTypes, blocks);
        grid = new GridImpl(blocks);
        return grid;
    }

    private void validateBlockTypeArray(LandBlockType[][] landBlockTypes) {
        int firstRowLength;
        validateArrayNotNullOrEmpty(landBlockTypes);
        firstRowLength = landBlockTypes[0].length;
        for (LandBlockType[] blockRow : landBlockTypes) {
            validateArrayNotNullOrEmpty(blockRow);
            if (blockRow.length != firstRowLength) {
                throw new InvalidInputException("Array is jagged!");
            }
        }
    }

    private <T> void validateArrayNotNullOrEmpty(T[] array) {
        if (array == null || array.length == 0){
            throw new InvalidInputException("Array is empty!");
        }
        for (T element : array) {
            if (element == null) {
                throw new InvalidInputException("Array contains null elements!");
            }
        }
    }

    private LandBlock[][] initBlockArray2D(LandBlockType[][] landBlockTypes) {
        LandBlock[][] blocks;
        int rows = landBlockTypes.length;
        int columns = landBlockTypes[0].length;
        blocks = new LandBlock[rows][columns];
        return blocks;
    }

    private void buildBlocksFromTypes(LandBlockType[][] landBlockTypes, LandBlock[][] blocks)  {
        LandBlockFactory factory = LandBlockFactoryImpl.getInstance();
        for (int row = 0; row < landBlockTypes.length; row++) {
            for (int column = 0; column < landBlockTypes[row].length; column++) {
                LandBlockType currentType = landBlockTypes[row][column];
                blocks[row][column] = factory.buildLandBlock(currentType);
            }
        }
    }

    public static GridFactoryImpl getInstance() {
        if (gridFactory == null) {
            gridFactory = new GridFactoryImpl();
        }
        return gridFactory;
    }
}

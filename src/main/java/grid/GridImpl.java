package grid;

import land.LandBlock;

public class GridImpl implements Grid{

    LandBlock[][] landBlocks;

    GridImpl(LandBlock[][] landBlocks) {
        this.landBlocks = landBlocks;
    }

    @Override
    public LandBlock getBlock(int row, int column) {
        return landBlocks[row][column];
    }

    @Override
    public int calculateUnclearLand() {
        int sum = 0;
        for (int row = 0; row < landBlocks.length; row++) {
            for (int column = 0; column < landBlocks[row].length; column++) {
                if (landBlocks[row][column].isClearable()){
                    sum++;
                }
            }
        }
        return sum;
    }

}

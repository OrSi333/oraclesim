package grid;

import land.LandBlockType;

public interface GridFactory {
    public Grid buildGridFromTypes(LandBlockType[][] landBlockTypes);
}

package grid;

import land.LandBlock;

public interface Grid {
    public LandBlock getBlock(int row, int column);
    public int calculateUnclearLand();
}

package vehicle;

import grid.Grid;
import land.LandBlock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static junit.framework.TestCase.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class TestVehicle {

    Vehicle vehicle;
    Position position;
    @Mock
    Grid gridMock;
    @Mock
    LandBlock plainLandMock;

    @Before
    public void setUp(){
        position = new Position(0, -1, Direction.East);
        plainLandMock = Mockito.mock(LandBlock.class);
        fillGridWithMocks();
        vehicle = new Vehicle();
        buildVehicle();
    }

    private void buildVehicle() {
        vehicle.setPosition(position);
        vehicle.setGrid(gridMock);
    }

    private void fillGridWithMocks() {
        Mockito.when(gridMock.getBlock(0,0)).thenReturn(plainLandMock);
        Mockito.when(gridMock.getBlock(0,1)).thenReturn(plainLandMock);
        Mockito.when(gridMock.getBlock(1,0)).thenReturn(plainLandMock);
        Mockito.when(gridMock.getBlock(1,1)).thenReturn(plainLandMock);
    }

    @Test
    public void testTurns(){
        LandBlock currentBlock;
        vehicle.goForward(1);
        vehicle.turnRight();
        vehicle.goForward(1);
        vehicle.turnLeft();
        Position finalPosition = new Position(1, 0, Direction.East);
        Position vehiclePosition = vehicle.getPosition();
        assertEquals(finalPosition, vehiclePosition);
    }

    @Test
    public void testFullRightSpin() {
        vehicle.goForward(1);
        for (int i = 0; i < 4; i++) {
            vehicle.goForward(1);
            vehicle.turnRight();
        }
        Position finalPosition = new Position(0, 0, Direction.East);
        Position vehiclePosition = vehicle.getPosition();
        assertEquals(finalPosition, vehiclePosition);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGoOutOfBounds(){
        Mockito.when(gridMock.getBlock(0,2)).thenThrow(new IndexOutOfBoundsException());
        vehicle.goForward(3);
    }

}

package vehicle;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static junit.framework.TestCase.*;

public class TestPosition {
    Position position;

    @Before
    public void setUp(){
        position = new Position(0, -1, Direction.East);
    }

    @Test
    public void testClone(){
        Position samePosition = position.clone();
        assertEquals(position, samePosition);
    }

    @Test
    public void testCloneHash(){
        Position samePosition = position.clone();
        assertEquals(position.hashCode(), samePosition.hashCode());
    }

    @Test
    public void testChangeClone(){
        Position differentPosition = position.clone();
        differentPosition.row = 1;
        assertNotSame(position, differentPosition);
    }

    @Test
    public void testCompateNonPosition(){
        Set<Object> set = new HashSet<>();
        set.add(position.row);
        set.add(position.column);
        set.add(position.facing);
        assertNotSame(position, set);
    }

    @Test
    public void testOtherType(){
        String other = "Other";
        assertFalse(position.equals(other));
    }

}

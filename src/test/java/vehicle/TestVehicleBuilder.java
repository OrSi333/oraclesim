package vehicle;

import grid.Grid;
import land.LandBlock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TestVehicleBuilder {

    VehicleDirector director;
    VehicleBuilder builder;
    @Mock
    Grid grid;
    @Mock
    LandBlock plainLandMock;

    @Before
    public void setUp(){
        builder = new VehicleBuilder();
        director = new VehicleDirector();
        grid = Mockito.mock(Grid.class);
    }

    @Test
    public void testBuildVehicle(){
        director.makeVehicle(builder, grid);
        Drivable drivable = builder.getDrivable();
        assertTrue(drivable instanceof Vehicle);
    }

    @Test
    public void testBuildAndMove(){
        director.makeVehicle(builder, grid);
        Drivable drivable = builder.getDrivable();
        Vehicle vehicle = (Vehicle) drivable;
        Mockito.when(grid.getBlock(0,0)).thenReturn(plainLandMock);
        vehicle.goForward(1);
        LandBlock currentBlock = vehicle.getCurrentBlock();
        assertEquals(plainLandMock, currentBlock);
    }

}

package vehicle;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        TestDirection.class,
        TestPosition.class,
        TestVehicle.class,
        TestVehicleBuilder.class
})

public class VehicleTestSuite {
}

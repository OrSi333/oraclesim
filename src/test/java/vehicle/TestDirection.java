package vehicle;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class TestDirection {

    Direction direction;

    @Before
    public void setUp(){
        direction = Direction.West;
    }

    @Test
    public void testLeft(){
        direction = direction.Left();
        assertEquals(Direction.South, direction);
    }

    @Test
    public void testRight(){
        direction = direction.Right();
        assertEquals(Direction.North, direction);
    }
}

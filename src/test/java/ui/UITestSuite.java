package ui;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        TestCharToLandAdapter.class,
        TestCharMapParser.class,
        TestCLIMessages.class,
        TestCLI.class
})
public class UITestSuite {
}

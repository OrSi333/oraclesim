package ui;

import bulldozer.BulldozerExpense;
import expense.RateCalculator;
import land.LandBlockType;
import land.LandBlockTypeAdapter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.BufferedReader;
import java.io.IOException;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

@RunWith(MockitoJUnitRunner.class)
public class TestCLI {

    SimulatorCLI cli;
    LandBlockTypeAdapter[][] siteMap;
    @Mock
    LandBlockTypeAdapter plain;
    @Mock
    LandBlockTypeAdapter protectedTree;
    int initUncleared;
    @Mock
    BufferedReader mockReader;


    @Before
    public void testCLI(){
        createMocks();
        buildSiteMap();
        initUncleared = 3;
        cli = new SimulatorCLI(siteMap);
        cli.userInput = mockReader;
    }

    private void createMocks(){
        Mockito.when(plain.getLandBlockType()).thenReturn(LandBlockType.Plain);
        Mockito.when(protectedTree.getLandBlockType()).thenReturn(LandBlockType.PreservedTree);
    }

    private void buildSiteMap() {
        siteMap = new LandBlockTypeAdapter[2][2];
        siteMap[0][0] = plain;
        siteMap[0][1] = plain;
        siteMap[1][0] = plain;
        siteMap[1][1] = protectedTree;
    }

    @Test
    public void testLoadMap(){
        cli.greetAndShowMap();
        RateCalculator<BulldozerExpense> expenseReport = cli.bulldozer.getExpenseReport();
        int unclearedLand = expenseReport.getQuantity(BulldozerExpense.unclearedSquares);

        assertEquals(initUncleared, unclearedLand);
    }

    @Test
    public void testAdvance(){
        setMockClientInput("a 1");
        cli.getUserCommand();
        checkCommandAmount( 1);
    }

    @Test
    public void testTurnLeft(){
        setMockClientInput("a 1");
        cli.getUserCommand();
        setMockClientInput("l");
        cli.getUserCommand();
        checkCommandAmount(2);
    }

    @Test
    public void testTurnRight(){
        setMockClientInput("a 1");
        cli.getUserCommand();
        setMockClientInput("r");
        cli.getUserCommand();
        setMockClientInput("a 1");
        cli.getUserCommand();
        checkCommandAmount( 3);
    }

    private void checkCommandAmount(int expectedCommandAmount){
        RateCalculator<BulldozerExpense> expenseReport = cli.bulldozer.getExpenseReport();
        int commandAmount = expenseReport.getQuantity(BulldozerExpense.communicationOverhead);

        assertEquals(expectedCommandAmount, commandAmount);

    }

    @Test
    public void testQuit(){
        setMockClientInput("q");
        cli.getUserCommand();
        boolean shouldTerminate = cli.shouldTerminate();

        assertTrue(shouldTerminate);
    }

    @Test
    public void testUnknownCommand(){
        failCommandAndContinue("notSupported");
    }

    @Test
    public void testAdvanceNotNumber(){
        failCommandAndContinue("a !");
    }


    @Test
    public void testAdvanceBack(){
        failCommandAndContinue("a -1");
    }

    private void failCommandAndContinue(String input){
        setMockClientInput(input);
        cli.getUserCommand();

        assertFalse(cli.isTrackLastCommand());

    }

    @Test
    public void testOutOfBound(){
        setMockClientInput("a 2");
        cli.getUserCommand();
        setMockClientInput("a 3");
        cli.getUserCommand();

        assertTrue(cli.shouldTerminate());
    }

    @Test
    public void testDestroyProtected(){
        setMockClientInput("a 2");
        cli.getUserCommand();
        setMockClientInput("r");
        cli.getUserCommand();
        setMockClientInput("a 1");
        cli.getUserCommand();

        assertTrue(cli.shouldTerminate());
    }

    private void setMockClientInput(String input){
        try {
            Mockito.when(mockReader.readLine()).thenReturn(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testNullInput(){
        setMockInputException(new NullPointerException());
        cli.getUserCommand();
        assertFalse(cli.isTrackLastCommand());
    }

    @Test
    public void testInputIOException(){
        setMockInputException(new IOException());
        cli.getUserCommand();
        assertFalse(cli.isTrackLastCommand());
    }

    private void setMockInputException(Exception exception){
        try {
            Mockito.when(mockReader.readLine()).thenThrow(exception);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package ui;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestCLIMessages {

    String[] commands;
    String[] fullCommands;
    String currentCommand;

    @Test
    public void testAdvance(){
        currentCommand = SimulatorCLIMessages.getFullCommand(SimulatorCLIMessages.advance);
        assertEquals(SimulatorCLIMessages.advanceFull, currentCommand);

    }

    @Test
    public void testTurnLeft(){
        currentCommand = SimulatorCLIMessages.getFullCommand(SimulatorCLIMessages.turnLeft);
        assertEquals(SimulatorCLIMessages.turnLeftFull, currentCommand);
    }

    @Test
    public void testTurnRight(){
        currentCommand = SimulatorCLIMessages.getFullCommand(SimulatorCLIMessages.turnRight);
        assertEquals(SimulatorCLIMessages.turnRightFull, currentCommand);
    }

    @Test
    public void testQuit(){
        currentCommand = SimulatorCLIMessages.getFullCommand(SimulatorCLIMessages.quit);
        assertEquals(SimulatorCLIMessages.quitFull, currentCommand);
    }
}

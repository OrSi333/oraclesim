package ui;

import land.LandBlockType;
import land.LandBlockTypeAdapter;
import land.TypeNotSupportedException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static junit.framework.TestCase.assertEquals;

public class TestCharMapParser {

    CharMapParser parser;
    String mapFilePath = "mapfile.txt";
    LandBlockTypeAdapter[][] siteMap;
    BufferedWriter mapWriter = null;
    Character notSupported = '\1';

    @Before
    public void setUp(){
        try {
            buildCharMapFile();
            parser = new CharMapParser(mapFilePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void buildCharMapFile() throws IOException{
        File mapFile = new File(mapFilePath);
        FileWriter mapFileWriter = new FileWriter(mapFile);
        mapWriter = new BufferedWriter(mapFileWriter);
    }


    @Test
    public void testParsePlain(){
        testParse(CharToLandAdapter.plain, LandBlockType.Plain);
    }

    @Test
    public void testParseRocky(){
        testParse(CharToLandAdapter.rocky, LandBlockType.Rocky);
    }

    @Test
    public void testParseTree(){
        testParse(CharToLandAdapter.tree, LandBlockType.Tree);
    }

    @Test
    public void testParsePreservedTree(){
        testParse(CharToLandAdapter.preservedTree, LandBlockType.PreservedTree);
    }

    @Test(expected = TypeNotSupportedException.class)
    public void testParseNotSupported(){
        writeAndParse(notSupported.toString());
    }

    private void testParse(Character toParse, LandBlockType expected){
        writeAndParse(toParse.toString());
        LandBlockTypeAdapter landTypeAdapter = siteMap[0][0];
        LandBlockType landType = landTypeAdapter.getLandBlockType();
        assertEquals(landType, expected);
    }

    private void writeAndParse(String message){
        try {
            mapWriter.write(message);
            mapWriter.flush();
            siteMap = parser.parseMap();
        } catch (IOException exception){
            exception.printStackTrace();
        }
    }

    @After
    public void tearDown(){
        try {
            parser.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

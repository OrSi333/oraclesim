package ui;

import land.LandBlockType;
import land.TypeNotSupportedException;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class TestCharToLandAdapter {
    CharToLandAdapter adapter;
    char[] charTypes;
    char unknown = '\0';

    @Before
    public void setUp(){
        charTypes = new char[] {
                CharToLandAdapter.plain,
                CharToLandAdapter.rocky,
                CharToLandAdapter.tree,
                CharToLandAdapter.preservedTree,
        };
    }

    @Test
    public void testPlain(){
        adapter = new CharToLandAdapter(charTypes[0]);
        LandBlockType type = adapter.getLandBlockType();
        assertEquals(LandBlockType.Plain, type);
    }
    @Test
    public void testRocky(){
        adapter = new CharToLandAdapter(charTypes[1]);
        LandBlockType type = adapter.getLandBlockType();
        assertEquals(LandBlockType.Rocky, type);
    }

    @Test
    public void testTree(){
        adapter = new CharToLandAdapter(charTypes[2]);
        LandBlockType type = adapter.getLandBlockType();
        assertEquals(LandBlockType.Tree, type);
    }

    @Test
    public void testPreservedTree(){
        adapter = new CharToLandAdapter(charTypes[3]);
        LandBlockType type = adapter.getLandBlockType();
        assertEquals(LandBlockType.PreservedTree, type);
    }

    @Test
    public void testToString(){
        adapter = new CharToLandAdapter(charTypes[0]);
        assertEquals(CharToLandAdapter.plain.toString(), adapter.toString());
    }

    @Test(expected = TypeNotSupportedException.class)
    public void testUnknownType(){
        adapter = new CharToLandAdapter(unknown);
    }
}

package bulldozer;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        TestBullzoder.class,
        TestBulldozerBuilder.class,
        TestBulldozerAPI.class
})


public class BulldozerTestSuite {
}

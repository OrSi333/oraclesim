package bulldozer;

import expense.RateCalculator;
import land.LandBlockType;
import land.LandBlockTypeAdapter;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class TestBulldozerAPI {

    BulldozerAPI bulldozer;
    LandBlockTypeAdapter[][] siteMap;
    RateCalculator<BulldozerExpense> expenseReport;

    @Before
    public void setUp(){
        bulldozer = new BulldozerSimulator();
        siteMap = new LandBlockTypeAdapterMock[2][2];
        fillSiteMap();
        bulldozer.loadSiteMap(siteMap);
    }

    private void fillSiteMap() {
        siteMap[0][0] = new LandBlockTypeAdapterMock(LandBlockType.Tree);
        siteMap[0][1] = new LandBlockTypeAdapterMock(LandBlockType.PreservedTree);
        siteMap[1][0] = new LandBlockTypeAdapterMock(LandBlockType.Plain);
        siteMap[1][1] = new LandBlockTypeAdapterMock(LandBlockType.Rocky);
    }

    @Test
    public void testClearSingleLand(){
        bulldozer.advance(1);
        assertUnclearLand(2);
    }

    @Test
    public void testClearAndTurn(){
        bulldozer.advance(1);
        bulldozer.turnRight();
        bulldozer.advance(1);
        bulldozer.turnLeft();
        bulldozer.advance(1);
        assertUnclearLand(0);
    }

    private void assertUnclearLand(int expected){
        expenseReport = bulldozer.getExpenseReport();
        int unclearLand = expenseReport.getQuantity(BulldozerExpense.unclearedSquares);
        assertEquals(expected, unclearLand);
    }
}

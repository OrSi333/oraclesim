package bulldozer;

import expense.RateCalculator;
import grid.Grid;
import grid.InvalidInputException;
import land.DestroyedPreservedException;
import land.LandBlock;
import land.LandBlockType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import vehicle.Direction;
import vehicle.Position;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static junit.framework.TestCase.*;

@RunWith(MockitoJUnitRunner.class)
public class TestBullzoder {
    Bulldozer bulldozer;
    Position position;
    @Mock
    Grid gridMock;
    @Mock
    LandBlock plainLandMock;
    @Mock
    LandBlock treeLandMock;
    @Mock
    RateCalculator<BulldozerExpense> rateMock;
    RateCalculator<BulldozerExpense> expense;
    Map<LandBlockType,Integer> fuelRate;
    Set<LandBlockType> paintDamage;

    @Before
    public void setUp(){
        position = new Position(0, -1, Direction.East);
        fillGridWithMocks();
        setLandMock();
        setFuelRate();
        setRateMock();
        setPaintDamage();
        bulldozer = new Bulldozer();
        buildBulldozer();
    }

    private void setPaintDamage() {
        paintDamage = new HashSet<>();
        paintDamage.add(LandBlockType.Tree);
    }

    private void setRateMock() {
        Mockito.doNothing().when(rateMock).add(BulldozerExpense.fuelUsage, 1);
    }

    private void setLandMock() {
        Mockito.when(plainLandMock.getType()).thenReturn(LandBlockType.Plain);
        Mockito.doNothing().when(plainLandMock).clear();
    }

    private void setFuelRate() {
        fuelRate = new HashMap<>();
        fuelRate.put(LandBlockType.Plain, 1);
        fuelRate.put(LandBlockType.Tree, 2);
        fuelRate.put(LandBlockType.PreservedTree, 2);
    }

    private void buildBulldozer() {
        bulldozer.setPosition(position);
        bulldozer.setGrid(gridMock);
        bulldozer.setFuelConsumption(fuelRate);
        bulldozer.setExpenseCalculator(rateMock);
        bulldozer.setPaintScratcher(paintDamage);
    }

    private void fillGridWithMocks() {
        Mockito.when(gridMock.getBlock(0,0)).thenReturn(plainLandMock);
        Mockito.when(gridMock.getBlock(0,1)).thenReturn(plainLandMock);
    }

    @Test
    public void testMoveSingleBlock(){
        bulldozer.goForward(1);
        Mockito.when(plainLandMock.isClear()).thenReturn(true);
        assertTrue(gridMock.getBlock(0,0).isClear());
    }

    @Test
    public void testPassOverBlock(){
        bulldozer.goForward(2);
        Mockito.when(plainLandMock.isClear()).thenReturn(false);
        assertFalse(gridMock.getBlock(0,0).isClear());
    }

    @Test
    public void testPassOverTree(){
        Mockito.when(treeLandMock.getType()).thenReturn(LandBlockType.Tree);
        Mockito.when(gridMock.getBlock(0,0)).thenReturn(treeLandMock);
        bulldozer.goForward(2);
        Position finalPosition = new Position(0, 1, Direction.East);
        Position vehiclePosition = bulldozer.getPosition();
        assertEquals(finalPosition, vehiclePosition);

    }

    @Test (expected = DestroyedPreservedException.class)
    public void testDestroyPreservedTree(){
        LandBlockType type = LandBlockType.PreservedTree;
        Mockito.when(treeLandMock.getType()).thenReturn(type);
        Mockito.doThrow(new DestroyedPreservedException(type)).when(treeLandMock).clear();
        Mockito.when(gridMock.getBlock(0,0)).thenReturn(treeLandMock);
        bulldozer.goForward(2);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGoOutOfBounds(){
        Mockito.when(gridMock.getBlock(0,2)).thenThrow(new IndexOutOfBoundsException());
        bulldozer.goForward(3);
    }

    @Test(expected = InvalidInputException.class)
    public void testGoBack(){
        bulldozer.goForward(-1);
    }

}

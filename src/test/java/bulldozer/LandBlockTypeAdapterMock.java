package bulldozer;

import land.LandBlockType;
import land.LandBlockTypeAdapter;

public class LandBlockTypeAdapterMock implements LandBlockTypeAdapter {

    LandBlockType type;

    public LandBlockTypeAdapterMock(LandBlockType type){
        this.type = type;
    }

    @Override
    public LandBlockType getLandBlockType() {
        return type;
    }
}

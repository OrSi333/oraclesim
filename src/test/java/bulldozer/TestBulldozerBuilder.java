package bulldozer;

import expense.RateCalculator;
import grid.Grid;
import land.LandBlock;
import land.LandBlockType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import vehicle.*;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TestBulldozerBuilder {
    BulldozerDirector director;
    BulldozerBuilder builder;
    @Mock
    Grid grid;
    @Mock
    LandBlock plainLandMock;

    @Before
    public void setUp(){
        builder = new BulldozerBuilder();
        director = new BulldozerDirector();
        Mockito.when(grid.getBlock(0,0)).thenReturn(plainLandMock);
        Mockito.when(plainLandMock.getType()).thenReturn(LandBlockType.Plain);
    }

    @Test
    public void testBuildBulldozer(){
        director.makeBulldozer(builder, grid);
        Drivable drivable = builder.getDrivable();
        assertTrue(drivable instanceof Bulldozer);
    }

    @Test
    public void testBuildAndMove(){
        Position endPosition = new Position(0,0, Direction.East);
        director.makeBulldozer(builder, grid);
        Drivable drivable = builder.getDrivable();
        Bulldozer bulldozer = (Bulldozer)drivable;
        Mockito.when(grid.getBlock(0,0)).thenReturn(plainLandMock);
        Mockito.doNothing().when(plainLandMock).clear();
        bulldozer.goForward(1);
        Position bulldozerPosition = bulldozer.getPosition();
        assertEquals(endPosition, bulldozerPosition);
    }

    @Test
    public void testExpense(){
        director.makeBulldozer(builder, grid);
        Drivable drivable = builder.getDrivable();
        Bulldozer bulldozer = (Bulldozer)drivable;
        bulldozer.goForward(1);
        RateCalculator<BulldozerExpense> expense = bulldozer.getExpenseReport();
        int fuelCost = expense.getTotalCost(BulldozerExpense.fuelUsage);
        assertEquals(1, fuelCost);
    }



}

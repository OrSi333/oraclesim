import bulldozer.BulldozerTestSuite;
import expense.ExpenseTestSuite;
import grid.GridTestSuite;
import land.LandTestSuite;
import log.LogTestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ui.UITestSuite;
import vehicle.VehicleTestSuite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        LandTestSuite.class,
        GridTestSuite.class,
        VehicleTestSuite.class,
        ExpenseTestSuite.class,
        BulldozerTestSuite.class,
        LogTestSuite.class,
        UITestSuite.class
})

public class FullModuleTestSuite {
}

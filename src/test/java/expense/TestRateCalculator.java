package expense;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;

import static junit.framework.TestCase.*;

@RunWith(MockitoJUnitRunner.class)
public class TestRateCalculator {
    RateCalculator<String> rateCalculator;
    String expense;
    int expenseQuantity;
    int price;

    @Before
    public void setUp(){
        rateCalculator = new RatCalculatorImpl<>();
        expense = "Expense";
        price = 7;
        expenseQuantity = 1;
        rateCalculator.setPrice(expense, price);
    }

    @Test
    public void testQuantity(){
        rateCalculator.add(expense, expenseQuantity);
        int quantity = rateCalculator.getQuantity(expense);
        assertEquals(expenseQuantity, quantity);

    }

    @Test
    public void testCost(){
        rateCalculator.add(expense, expenseQuantity);
        int cost = rateCalculator.getTotalCost(expense);
        assertEquals(price * expenseQuantity, cost);
    }

    @Test
    public void testClone(){
        rateCalculator.add(expense, expenseQuantity);
        RateCalculator<String> clone = rateCalculator.clone();
        rateCalculator.add(expense, expenseQuantity);
        int myQuantity = rateCalculator.getQuantity(expense);
        int cloneQuantity = clone.getQuantity(expense);
        assertNotSame(myQuantity, cloneQuantity);
    }

    @Test
    public void testAddExisting(){
        rateCalculator.add(expense, expenseQuantity);
        rateCalculator.add(expense, expenseQuantity);
        int quantity = rateCalculator.getQuantity(expense);
        assertEquals(expenseQuantity * 2, quantity);
    }

    @Test
    public void testNonExistingExpense(){
        int quantity = rateCalculator.getQuantity(expense);
        assertEquals(0, quantity);
    }

    @Test(expected = PriceNotFoundException.class)
    public void testPriceNotFound(){
        String noPriceExpense = "NoPrice";
        rateCalculator.add(noPriceExpense, 1);
        rateCalculator.getTotalCost(noPriceExpense);
    }

    @Test
    public void testGetKeySet(){
        int expenseQuantity = 1;
        rateCalculator.add(expense, expenseQuantity);
        Set<String> allPrices = rateCalculator.getKeySet();
        assertTrue(allPrices.contains(expense));
    }
}

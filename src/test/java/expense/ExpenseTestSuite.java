package expense;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        TestRateCalculator.class
})

public class ExpenseTestSuite {
}

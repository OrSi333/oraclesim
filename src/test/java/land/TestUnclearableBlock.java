package land;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertFalse;

public class TestUnclearableBlock {

    UnclearableLandBlock landBlock;

    @Before
    public void setUp(){
        landBlock = new UnclearableLandBlock(LandBlockType.Plain);
    }

    @Test(expected = DestroyedPreservedException.class)
    public void testFailCleanBlock(){
        landBlock.clear();
    }

    @Test
    public void testBlockIsClear(){
        assertFalse(landBlock.isClear());
    }
}

package land;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        TestClearableLandBlock.class,
        TestUnclearableBlock.class,
        TestLandBlockFactory.class
})
public class LandTestSuite {
}

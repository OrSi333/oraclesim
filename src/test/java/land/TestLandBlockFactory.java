package land;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class TestLandBlockFactory {

    LandBlockFactory landBlockFactory;
    LandBlock landBlock;

    @Before
    public void setUp(){
        landBlockFactory = LandBlockFactoryImpl.getInstance();
    }

    @Test
    public void testBuildPlain(){
        buildLandAndValidateType(LandBlockType.Plain);
    }

    @Test
    public void testBuildRocky(){
        buildLandAndValidateType(LandBlockType.Rocky);

    }

    @Test
    public void testBuildTree(){
        buildLandAndValidateType(LandBlockType.Tree);
    }

    @Test
    public void testBuildPreservedTree(){
        buildLandAndValidateType(LandBlockType.PreservedTree);
    }

    private void buildLandAndValidateType(LandBlockType landBlockType){
        landBlock = landBlockFactory.buildLandBlock(landBlockType);
        assertEquals(landBlockType, landBlock.getType());
    }

    @Test (expected = TypeNotSupportedException.class)
    public void testBuildUnsupportedType() {
        landBlockFactory.buildLandBlock(LandBlockType.Clear);
    }
}

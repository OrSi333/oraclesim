package land;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class TestClearableLandBlock {
    LandBlock landBlock;

    @Before
    public void setUp(){
        landBlock = new ClearableLandBlock(LandBlockType.Plain);
    }

    @Test
    public void testLandBlockClear() {
        landBlock.clear();
        assertTrue(landBlock.isClear());
    }

    @Test (expected = UnableToClearLandException.class)
    public void testFailToClearBlock() {
        landBlock.clear();
        landBlock.clear();
    }

}

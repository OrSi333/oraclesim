package log;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static junit.framework.TestCase.*;

public class TestFileLogger {
    final String filePath = "tmpfile.txt";
    final String message = "message";
    FileLogger logger;
    File file;
    BufferedReader reader;

    @Before
    public void setUp(){
        file = new File(filePath);
        try {
            logger = new FileLogger(filePath);
            reader = new BufferedReader(new FileReader(file));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testWriteLog(){
        String read = null;
        try {
            logger.writeMessageToLog(message);
            read = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertEquals(message, read);
    }

    @Test
    public void testWriteException(){
        Exception exception = new Exception(message);
        String read = null;
        try {
            logger.writeExceptionToLog(exception);
            read = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertTrue(read.contains(message));
    }

    @Test(expected = IOException.class)
    public void testWriteAfterClose() throws IOException{
        try {
            logger.close();
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
        logger.writeMessageToLog(message);
    }


    @Test(expected = IOException.class)
    public void testFileRemoved() throws IOException{
        try {
            close();
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
        logger.writeMessageToLog(message);
    }

    @After
    public void tearDown(){
        try {
            close();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    private void close() throws IOException{
        if (reader.ready()) {
            reader.close();
        }
        if (logger.ready()) {
            logger.close();
        }
        if (file.exists()) {
            file.delete();
        }
    }
}

package grid;

import land.LandBlock;
import land.LandBlockType;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static junit.framework.TestCase.assertEquals;

public class TestGridFactory {

    GridFactory gridFactory;
    LandBlockType[][] landBlockTypes;
    Grid grid;

    @Before
    public void setUp() {
        gridFactory = GridFactoryImpl.getInstance();
        landBlockTypes = new LandBlockType[2][2];
    }

    @Test
    public void testBuildPlainGrid(){
        fillBlocksWithPlain();
        grid = gridFactory.buildGridFromTypes(landBlockTypes);
        LandBlock currentBlock = grid.getBlock(0,0);
        assertEquals(LandBlockType.Plain, currentBlock.getType());
    }

    private void fillBlocksWithPlain() {
        for (int row = 0; row < landBlockTypes.length; row++) {
            Arrays.fill(landBlockTypes[row], LandBlockType.Plain);
        }
    }

    @Test (expected = InvalidInputException.class)
    public void testFailBuildFromNull(){
        grid = gridFactory.buildGridFromTypes(null);
    }

    @Test (expected = InvalidInputException.class)
    public void testFailBuildFromEmpty(){
        grid = gridFactory.buildGridFromTypes(landBlockTypes);
    }

    @Test (expected = InvalidInputException.class)
    public void testFailBuildFromJaggedArray(){
        buildJaggedArray();
        grid = gridFactory.buildGridFromTypes(landBlockTypes);
    }

    private void buildJaggedArray() {
        landBlockTypes[0] = new LandBlockType[2];
        Arrays.fill(landBlockTypes[0], LandBlockType.Plain);
        landBlockTypes[1] = new LandBlockType[1];
        Arrays.fill(landBlockTypes[1], LandBlockType.Plain);
    }
}

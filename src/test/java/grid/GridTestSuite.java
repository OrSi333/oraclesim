package grid;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        TestGrid.class,
        TestGridFactory.class,
})
public class GridTestSuite {
}

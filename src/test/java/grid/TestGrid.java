package grid;

import land.LandBlock;
import land.LandBlockType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static junit.framework.TestCase.*;

@RunWith(MockitoJUnitRunner.class)
public class TestGrid {

    Grid grid;
    LandBlock[][] mockBlockArray2D;
    @Mock
    LandBlock plainBlock;
    @Mock
    LandBlock unclearableBlock;

    @Before
    public void setUp(){
        mockBlockArray2D = new LandBlock[2][2];
        prepareMock();
        fillArrayWithMock();
        grid = new GridImpl(mockBlockArray2D);
    }

    private void prepareMock() {
        Mockito.when(plainBlock.getType()).thenReturn(LandBlockType.Plain);
    }

    private void fillArrayWithMock() {
        mockBlockArray2D[0][0] = plainBlock;
        mockBlockArray2D[0][1] = plainBlock;
        mockBlockArray2D[1][0] = plainBlock;
        mockBlockArray2D[1][1] = unclearableBlock;
    }

    @Test
    public void testGetBlock(){
        LandBlock plain = grid.getBlock(0,0);
        assertEquals(LandBlockType.Plain, plain.getType());
    }

    @Test
    public void testUnclearBlocks(){
        Mockito.when(plainBlock.isClearable()).thenReturn(true);
        Mockito.when(unclearableBlock.isClearable()).thenReturn(false);
        int unclearSum = grid.calculateUnclearLand();
        assertEquals(3, unclearSum);
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void testGetBlockOutsideGrid(){
        LandBlock invalidBlock = grid.getBlock(7,7);
    }

}
